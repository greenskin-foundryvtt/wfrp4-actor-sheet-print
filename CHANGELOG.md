## In development

## 1.2.0

### New features

- HTML Responsive Export Characters sheet
- Add contextual menu to actor to HTML responsive Export Characters sheet

## 1.1.0

- Add contextual menu to actor to PDF Export Characters sheet

## 1.0.0

### New features

- PDF Export Characters sheet
