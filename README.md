# WFRP 4 Actor Sheet Print

This module allow to export characters sheet on a friendly print pdf or a responsive html.

# Authors

Skeroujvapluvit

## Supported language

- All

## System and dependencies

This module need the Warhammer Fantasy Roleplay 4e system

- System : https://foundryvtt.com/packages/wfrp4e

### Optional dependencies

### External dependencies

- jsPDF : https://github.com/parallax/jsPDF

## Usage

A print buton is added on top of sheet to export as pdf

![Print button](media/actor-sheet-print-button.png)

A Contextual menu is added on Actor menu, one for pdf, an other for html

![Actor button](media/actor-sheet-print-actor-button.png)

## Comming soon
