import jsPDF, { jsPDFOptions } from 'jspdf';
import { AbstractElement } from './elements/abstract-element';
import { LABEL_SIZE, MARGINS, TEXT_SIZE } from './constants';
import { AbstractBuilder } from './abstract-builder';
import { Util } from './util';
import { GenerateTypeEnum } from './elements/generate-type.enum';

export class PdfBuilder extends AbstractBuilder {
  public doc: jsPDF;

  constructor(options: jsPDFOptions) {
    super();
    this.doc = new jsPDF(options);
  }

  public getLabelledRowHeight(): number {
    return Util.getHeightFromPx(this.doc, TEXT_SIZE + LABEL_SIZE) + 1;
  }

  public save(name: string) {
    this.doc.save(`${name}.pdf`);
  }

  public build(elements: AbstractElement[]) {
    const finalElements: AbstractElement[] = [];
    for (const element of elements.filter((el) => el.context.isPdf)) {
      element.prepareRender(this.doc);
      finalElements.push(...element.getElements());
    }
    finalElements.sort((a, b) => {
      return a.y - b.y;
    });

    const pageHeight = this.doc.internal.pageSize.height;
    const yMax = pageHeight - MARGINS.bottom;
    const pages: AbstractElement[][] = [];

    for (const element of finalElements) {
      let indexPage = 0;
      let currentY = element.y;
      const height = element.getCheckNewPageHeight(this.doc);
      if (currentY + height > yMax) {
        while (currentY + height > yMax) {
          indexPage++;
          currentY = currentY - yMax + MARGINS.bottom;
          if (currentY + height <= yMax) {
            if (pages[indexPage] == null) {
              pages[indexPage] = [];
            }
            element.y = currentY;
            pages[indexPage].push(element);
          }
        }
      } else {
        if (pages[indexPage] == null) {
          pages[indexPage] = [];
        }
        pages[indexPage].push(element);
      }
    }

    let i = 0;
    for (const page of pages) {
      i++;
      for (const element of page) {
        element.render(this.doc);
      }
      if (i < pages.length) {
        this.doc.addPage();
      }
    }
  }

  public getImageScale(): number {
    return 1;
  }

  public getGenerateType(): GenerateTypeEnum {
    return GenerateTypeEnum.PDF;
  }
}
