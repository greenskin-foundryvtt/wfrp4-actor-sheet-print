import { AbstractElement } from './abstract-element';
import { IContext } from './context';
import { GenerateTypeEnum } from './generate-type.enum';

export abstract class AbstractContainerElement extends AbstractElement {
  public pdfElements: AbstractElement[] = [];
  public htmlElements: AbstractElement[] = [];
  public contextElements: AbstractElement[] = [];

  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    maxWidth: number | undefined,
    elements: AbstractElement[] = [],
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, maxWidth, context);
    this.htmlElements = this.context.isHtml
      ? elements.filter((el) => el.context.isHtml)
      : [];
    this.pdfElements = this.context.isPdf
      ? elements.filter((el) => el.context.isPdf)
      : [];
    this.contextElements =
      this.globalType === GenerateTypeEnum.HTML
        ? this.htmlElements
        : this.pdfElements;
  }

  public getElements(): AbstractElement[] {
    const elements: AbstractElement[] = [];
    for (const element of this.contextElements) {
      elements.push(...element.getElements());
    }
    return elements;
  }
}
