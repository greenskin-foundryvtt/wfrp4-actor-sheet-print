import jsPDF from 'jspdf';
import { MARGINS } from '../constants';
import { IContext } from './context';
import { GenerateTypeEnum } from './generate-type.enum';

export abstract class AbstractElement {
  public static readonly DEFAULT_CONTEXT: IContext = {
    isHtml: true,
    isPdf: true,
    name: 'element',
  };

  public globalType: GenerateTypeEnum;
  public x: number;
  public y: number;
  public maxWidth?: number;
  public context: IContext = AbstractElement.DEFAULT_CONTEXT;

  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    maxWidth?: number | undefined,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    this.globalType = globalType;
    this.x = x >= MARGINS.left ? x : MARGINS.left;
    this.y = y >= MARGINS.top ? y : MARGINS.top;
    this.maxWidth = maxWidth;
    this.context = {
      ...this.context,
      ...context,
    };
  }

  public getHeightFromPx(doc: jsPDF, size: number) {
    return size / doc.internal.scaleFactor;
  }

  public getPxFromSize(doc: jsPDF, size: number) {
    return size * doc.internal.scaleFactor;
  }

  protected updateMaxWidth(maxWidth?: number) {
    this.maxWidth = maxWidth;
  }

  public abstract prepareRender(doc: jsPDF, maxWidth?: number): jsPDF;

  public abstract render(doc: jsPDF, maxWidth?: number): jsPDF;

  public abstract renderHtml(
    doc: Document,
    parent: HTMLElement,
    cssRules: string[],
    sheet: HTMLStyleElement
  ): Document;

  public abstract getHeight(doc?: jsPDF): number;

  public abstract getCheckNewPageHeight(doc?: jsPDF): number;

  public abstract getElements(): AbstractElement[];
}
