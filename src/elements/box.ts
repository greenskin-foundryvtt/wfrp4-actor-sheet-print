import { AbstractElement } from './abstract-element';
import jsPDF from 'jspdf';
import { IContext } from './context';
import { GenerateTypeEnum } from './generate-type.enum';

export class Box extends AbstractElement {
  public w: number;
  public h: number;

  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    w: number,
    h: number,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, w, context);
    this.w = w;
    this.h = h;
  }

  public prepareRender(doc: jsPDF, _maxWidth?: number): jsPDF {
    return doc;
  }

  public render(doc: jsPDF, _maxWidth?: number): jsPDF {
    doc.rect(this.x, this.y, this.w, this.h);
    return doc;
  }

  public renderHtml(
    doc: Document,
    parent: HTMLElement,
    cssRules: string[],
    sheet: HTMLStyleElement
  ): Document {
    const div = doc.createElement('div');
    const css = `box-${this.w ?? 0}-${this.h ?? 0}`;
    div.classList.add(`box`);
    div.classList.add(css);
    div.classList.add(this.context.name);
    if (!cssRules.includes(css)) {
      cssRules.push(css);
      let rule = 'width: 100%;';
      if (this.w > 0) {
        rule += `max-width: ${this.w}px;`;
        rule += `min-width: ${this.w}px;`;
      }
      if (this.h > 0) {
        rule += `max-height: ${this.h}px;`;
        rule += `min-height: ${this.h}px;`;
      }
      sheet.innerHTML += ` .${css} { ${rule} }`;
    }
    parent.append(div);
    return doc;
  }

  public getHeight(_doc): number {
    return this.h;
  }

  public getCheckNewPageHeight(doc?: jsPDF): number {
    return this.getHeight(doc);
  }

  public getElements(): AbstractElement[] {
    return [this];
  }
}
