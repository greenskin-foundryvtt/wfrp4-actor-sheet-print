import jsPDF, { TextOptionsLight } from 'jspdf';
import { Text } from './text';
import {
  HTML_LABEL_SIZE,
  HTML_TEXT_SIZE,
  i18nLocalize,
  LABEL_SIZE,
  TEXT_SIZE,
} from '../constants';
import { AbstractElement } from './abstract-element';
import { IContext } from './context';
import { GenerateTypeEnum } from './generate-type.enum';

export class LabelledText extends Text {
  public label: string;
  public labelOptions?: TextOptionsLight;

  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    label: string,
    text: string,
    textOptions?: TextOptionsLight,
    labelOptions?: TextOptionsLight,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, text, textOptions, context);
    this.label = label;
    this.labelOptions = labelOptions;
    const textMaxWidth = this.textOptions?.maxWidth ?? 0;
    const labelMaxWidth = this.labelOptions?.maxWidth ?? 0;
    const maxWidth = Math.max(textMaxWidth, labelMaxWidth);
    this.updateMaxWidth(maxWidth);
  }

  public prepareRender(doc: jsPDF, maxWidth?: number): jsPDF {
    this.updateMaxWidth(maxWidth);
    return super.prepareRender(doc, maxWidth);
  }

  public render(doc: jsPDF, _maxWidth?: number): jsPDF {
    const yLabel = this.y + this.getHeightFromPx(doc, LABEL_SIZE);
    const yText = yLabel + this.getHeightFromPx(doc, TEXT_SIZE) + 1;
    doc
      .setFontSize(LABEL_SIZE)
      .text(i18nLocalize(this.label), this.x, yLabel, this.labelOptions)
      .setFontSize(TEXT_SIZE)
      .text(this.text, this.x, yText, this.textOptions);
    return doc;
  }

  public renderHtml(
    doc: Document,
    parent: HTMLElement,
    cssRules: string[],
    sheet: HTMLStyleElement
  ): Document {
    const div = doc.createElement('div');
    div.classList.add(`column`);
    div.classList.add(`labelled-text`);
    div.classList.add(this.context.name);
    const label = doc.createElement('p');
    const text = doc.createElement('p');
    const labelCss = `label-${LABEL_SIZE}`;
    const textCss = `text-${TEXT_SIZE}`;
    label.classList.add(labelCss);
    text.classList.add(textCss);
    if (!cssRules.includes(labelCss)) {
      cssRules.push(labelCss);
      sheet.innerHTML += ` .${labelCss} { font-size: ${HTML_LABEL_SIZE}rem; margin: 0; }`;
    }
    if (!cssRules.includes(textCss)) {
      cssRules.push(textCss);
      sheet.innerHTML += ` .${textCss} { font-size: ${HTML_TEXT_SIZE}rem; margin-top: 0.5rem; margin-bottom: 0.5rem; }`;
      sheet.innerHTML += ` .labelled-text { margin-top: 0.5rem; margin-bottom: 0.5rem; }`;
      sheet.innerHTML += ` .labelled-text > .${textCss} { font-size: ${HTML_TEXT_SIZE}rem; margin: 0; }`;
    }
    label.innerHTML = i18nLocalize(this.label);
    text.innerHTML = i18nLocalize(this.text);
    div.append(label);
    div.append(text);
    parent.append(div);
    return doc;
  }

  protected updateMaxWidth(maxWidth?: number) {
    if (maxWidth != null && maxWidth > 0) {
      this.maxWidth = maxWidth;
      const textOpts: TextOptionsLight = this.textOptions ?? {};
      const labelOpts: TextOptionsLight = this.labelOptions ?? {};
      textOpts.maxWidth = maxWidth;
      labelOpts.maxWidth = maxWidth;
      this.textOptions = textOpts;
      this.labelOptions = labelOpts;
    }
  }

  public getHeight(doc): number {
    return this.getHeightFromPx(doc, TEXT_SIZE + LABEL_SIZE) + 1;
  }

  public getCheckNewPageHeight(doc?: jsPDF): number {
    return this.getHeight(doc);
  }

  public getElements(): AbstractElement[] {
    return [this];
  }
}
