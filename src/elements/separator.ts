import { AbstractElement } from './abstract-element';
import jsPDF from 'jspdf';
import { MARGINS } from '../constants';
import { IContext } from './context';
import { GenerateTypeEnum } from './generate-type.enum';

export class Separator extends AbstractElement {
  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    maxWidth?: number | undefined,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, maxWidth, context);
  }

  public getHeight(_doc?: jsPDF): number {
    return 0.5;
  }

  public getCheckNewPageHeight(doc?: jsPDF): number {
    return this.getHeight(doc);
  }

  public prepareRender(doc: jsPDF, _maxWidth?: number): jsPDF {
    return doc;
  }

  public render(doc: jsPDF, maxWidth?: number): jsPDF {
    const pageWidth = doc.internal.pageSize.width;
    const maxPageWidth = pageWidth - MARGINS.left - MARGINS.right;
    const finalWidth = Math.min(
      maxWidth ?? this.maxWidth ?? maxPageWidth,
      maxPageWidth
    );

    doc.setLineWidth(0.25);
    doc.line(this.x, this.y, this.x + finalWidth, this.y);

    return doc;
  }

  public renderHtml(
    doc: Document,
    parent: HTMLElement,
    _cssRules: string[],
    _sheet: HTMLStyleElement
  ): Document {
    const div = doc.createElement('div');
    div.classList.add(`separator`);
    div.classList.add(this.context.name);
    parent.append(div);
    return doc;
  }

  public getElements(): AbstractElement[] {
    return [this];
  }
}
