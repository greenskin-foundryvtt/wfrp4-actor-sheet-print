import { AbstractElement } from './abstract-element';
import jsPDF, { TextOptionsLight } from 'jspdf';
import { HTML_TEXT_SIZE, i18nLocalize, TEXT_SIZE } from '../constants';
import { Text } from './text';
import { IContext } from './context';
import { GenerateTypeEnum } from './generate-type.enum';

export class MultilineText extends Text {
  private nbrLine = 1;

  constructor(
    globalType: GenerateTypeEnum,
    x: number,
    y: number,
    text: string,
    textOptions?: TextOptionsLight,
    context: Partial<IContext> = AbstractElement.DEFAULT_CONTEXT
  ) {
    super(globalType, x, y, text, textOptions, context);
  }

  public prepareRender(doc: jsPDF, maxWidth?: number): jsPDF {
    doc.setFontSize(TEXT_SIZE);
    this.updateMaxWidth(maxWidth);
    let finalText: string[] = [i18nLocalize(this.text)];
    if (this.maxWidth != null) {
      finalText = doc.splitTextToSize(finalText[0], this.maxWidth);
    }
    this.nbrLine = finalText.length;
    return doc;
  }

  public render(doc: jsPDF, _maxWidth?: number): jsPDF {
    const yText = this.y + this.getHeightFromPx(doc, TEXT_SIZE);
    doc.setFontSize(TEXT_SIZE).text(this.text, this.x, yText, this.textOptions);
    return doc;
  }

  public renderHtml(
    doc: Document,
    parent: HTMLElement,
    cssRules: string[],
    sheet: HTMLStyleElement
  ): Document {
    const text = doc.createElement('p');
    const css = `text-${TEXT_SIZE}`;
    text.classList.add(`multiline-text`);
    text.classList.add(css);
    text.classList.add(this.context.name);
    if (!cssRules.includes(css)) {
      cssRules.push(css);
      sheet.innerHTML += ` .${css} { font-size: ${HTML_TEXT_SIZE}rem }`;
      sheet.innerHTML += ` .labelled-text > .${css} { font-size: ${HTML_TEXT_SIZE}rem; margin: 0; }`;
    }
    text.innerHTML = i18nLocalize(this.text);
    parent.append(text);
    return doc;
  }

  public getHeight(doc): number {
    return this.getHeightFromPx(doc, TEXT_SIZE) * this.nbrLine;
  }

  public getElements(): AbstractElement[] {
    return [this];
  }
}
