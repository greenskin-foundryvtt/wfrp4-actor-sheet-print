import { AbstractElement } from './elements/abstract-element';
import { GenerateTypeEnum } from './elements/generate-type.enum';

export abstract class AbstractBuilder {
  public abstract build(elements: AbstractElement[]);

  public abstract getLabelledRowHeight(): number;

  public abstract save(name: string);

  public abstract getImageScale(): number;

  public abstract getGenerateType(): GenerateTypeEnum;
}
